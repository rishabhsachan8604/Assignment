﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Assignment.Models;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Http;

namespace Assignment.Controllers
{
    public class HomeController : Controller
    {
        SqlConnection cn = new SqlConnection(@"Data Source=RAHULRATHAUR-PC\SQLEXPRESS;Initial Catalog=Assignment;Integrated Security=True");
        [HttpGet]
        public IActionResult Login()
        {
            return View("Login");
        }
        [HttpPost]
        public IActionResult Login(Login login)
        {
            cn.Open();
            SqlCommand cmd = new SqlCommand("select * from Login where user_name='" + login.Registration_no + "' and password='" + login.Password + "'",cn);
            SqlDataReader dr=cmd.ExecuteReader();
            bool va = dr.Read();
            cn.Close();
            if (va)
            {
                HttpContext.Session.SetString("login",login.Registration_no); 
                return RedirectToAction("Index");
            }
            return View();
        }
        [HttpGet]
        public IActionResult Index()
        {
            string name = HttpContext.Session.GetString("login");
            if (name!=null)
            {
                ViewBag.Name = name;
                cn.Open();
                List<Data> ls = new List<Data>();
                SqlCommand cmd = new SqlCommand("select * from Data", cn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ls.Add(new Data { Seq = (ls.Count + 1), id = Convert.ToInt32(dr["id"]), Nane = dr["Name"].ToString(), Last_update = Convert.ToDateTime(dr["last_update"]) });
                }
                cn.Close();
                return View(ls);
            }
            return View("Login");
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            cn.Open();
            Data data= new Data();
            SqlCommand cmd = new SqlCommand("select * from Data where id='" + id + "'", cn);
            SqlDataReader dr=cmd.ExecuteReader();
            if (dr.Read())
            {
                data.Nane = dr["Name"].ToString();
                data.id =id;
            }
            return View(data);
        }
        [HttpPost]
        public IActionResult Edit(string Name)
        {
            cn.Open();
            SqlCommand cmd = new SqlCommand("insert into Data values('"+Name+"',getdate())", cn);
            if (cmd.ExecuteNonQuery()>0)
            {
                List<Data> ls = new List<Data>();
                cmd = new SqlCommand("select * from Data", cn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ls.Add(new Data { Seq = (ls.Count + 1), id = Convert.ToInt32(dr["id"]), Nane = dr["Name"].ToString(), Last_update = Convert.ToDateTime(dr["last_update"]) });
                }
                cn.Close();
                return Json(ls);
            }
            return RedirectToAction("Edit");
        }
        [HttpGet]
        public IActionResult Delete(int id)
        {
            cn.Open();
            SqlCommand cmd = new SqlCommand("delete from data where id='" + id + "'", cn);
            cmd.ExecuteNonQuery();
            return RedirectToAction("Index", "Home");
        }
        public IActionResult df()
        {
            return View();
        }
        public IActionResult Save_seq(List<string> data1)
        {
            cn.Open();
            SqlCommand cmd = new SqlCommand("delete from data", cn);
            cmd.ExecuteNonQuery();
            foreach(string Name in data1) {
                cmd = new SqlCommand("insert into Data values('" + Name + "',getdate())",cn);
                cmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index", "Home");
        }
        //public IActionResult About()
        //{
        //    ViewData["Message"] = "Your application description page.";

        //    return View();
        //}

        //public IActionResult Contact()
        //{
        //    ViewData["Message"] = "Your contact page.";

        //    return View();
        //}

        //public IActionResult Privacy()
        //{
        //    return View();
        //}

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
